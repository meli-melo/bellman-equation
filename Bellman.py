import matplotlib.pyplot as plt
from graphviz import Digraph


f = Digraph('map', filename='graphTest')
f.attr(rankdir='LR')

f.attr('node', shape='doublecircle')
f.node('Start')
f.node('Lose')
f.node('Win')

f.attr('node', shape='circle')
f.edge('Start', 'A',)
f.edge('A', 'B',)
f.edge('A', 'I')
f.edge('B', 'C', label='SS(b)')
f.edge('C', 'D', label='SS(a)')
f.edge('D', 'E', label='S(A)')
f.edge('E', 'Win', label='S(b)')
f.edge('E', 'G', label='S(a)')
f.edge('G', 'E', label='S(b)')
f.edge('G', 'Lose', label='S(a)')
f.edge('G', 'J', label='S(b)')
f.edge('I', 'A', label='S(a)')
f.edge('I', 'J', label='S(b)')
f.edge('J', 'G', label='S(a)')
f.edge('J', 'I', label='S(a)')
f.edge('J', 'K', label='S(a)')
f.edge('K', 'J', label='S(a)')
f.edge('K', 'Lose', label='S(a)')

f.view()